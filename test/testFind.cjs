const demoArray = require('../demoData.cjs');
const findMethodFunction = require('../find.cjs');


function callBack(ele, checkElement) {

    return ele > checkElement;

}

const solutionOfFind = findMethodFunction(demoArray, callBack);
console.log(solutionOfFind);

