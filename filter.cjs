
function filterFunction(data, callBack) {

    if (!Array.isArray(data)) {
        return;
    }
    if (data == null || data == undefined || data.length == 0) {
        return;
    }
    
    let ouptutArray = [];

    for (let index=0;index<data.length;index++) {
        let result = (callBack(data[index], index,data));
        if (result === true) {
            ouptutArray.push(data[index]);
        }


    }
    return ouptutArray;
}
module.exports = filterFunction;