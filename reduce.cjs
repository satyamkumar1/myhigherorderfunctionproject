

function reduceFunction(data, callback, intialValue) {


    if (data.length == 0) {
        return;
    }

    if (!Array.isArray(data)) {
        return;
    }
    if (data == undefined || data == null) {
        return;
    }
    let flag = true;
    if (intialValue == undefined) {
        intialValue = data[0];
        flag = false;
    }

    let ans = intialValue;
    for (let index = 0; index < data.length; index++) {
        if (!flag && index == 0) {
            index = 1;

        }
        ans = (callback(ans, data[index], index, data));
    }


    return ans;
}
module.exports = reduceFunction;