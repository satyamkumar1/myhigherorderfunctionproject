const demoArray = require('../demoData.cjs');
const each = require('../each.cjs');


function callBack(element, index) {

    console.log(`element: ${element} of an array at index ${index}`);

}

const solutionOfForEach = each(demoArray, callBack);

