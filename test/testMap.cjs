const demoArray = require('../demoData.cjs');
const map = require('../map.cjs');

function callBackFunction(element, index,arrayData) {
//console.log(element, index,arrayData);
    return element * 2;

}

const solutionOfMap = map(demoArray, callBackFunction);
console.log(solutionOfMap);
