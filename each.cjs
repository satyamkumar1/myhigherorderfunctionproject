
function each(data, callBack) {


    if (!Array.isArray(data)) {
        return;
    }
    if (data == null || data == undefined || data.length == 0) {
        return;
    }

    for (let index in data) {
        (callBack(data[index], index));
    }

}
module.exports = each;