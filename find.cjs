
function findFunction(data, callBack) {

    if (!Array.isArray(data)) {
        return;
    }
    if (data == null || data == undefined || data.length == 0) {
        return;
    }
    let checkElement = 4;

    for (let index in data) {
        let result = (callBack(data[index], checkElement));
        if (result) {
            return data[index];
        }


    }
    return undefined;
}
module.exports = findFunction;