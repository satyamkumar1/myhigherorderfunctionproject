const demoArray = require('../demoData.cjs');
const filterFunction = require('../filter.cjs');


function callBack(element, index, data) {

    return element > 1;

}

const solutionOfFilter = filterFunction(demoArray, callBack);
console.log(solutionOfFilter);

