

function mapFunction(data, callBackFunction) {

    if (!Array.isArray(data)) {
        return [];
    }
    if (data == undefined || data == null || data.length == 0) {
        return;
    }


    let ans = [];
    for (let index=0;index<data.length;index++) {
        ans.push(callBackFunction(data[index], index, data));
    }

    
    return ans;
}
module.exports = mapFunction;