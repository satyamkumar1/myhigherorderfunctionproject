
function flattenFunction(data, depth) {

    // if (!Array.isArray(data)) {
    //     return;
    // }
    // if (data == null || data == undefined) {
    //     return;
    // }
    if (depth == undefined) {
        depth = 1;
    }

    let ans = [];
    for (let index = 0; index < data.length; index++) {

        if (Array.isArray(data[index]) && depth > 0) {
            ans = ans.concat(flattenFunction(data[index], depth - 1));
        }
        else if (index in data) {
            ans.push(data[index]);
        }
    }

    return ans;
}
module.exports = flattenFunction;